const {
    MessageEmbed
} = require('discord.js')

exports.run = async (client, message, args) => {
    const exampleEmbed = new MessageEmbed()
        .setTitle('Back+ Help')
        .setDescription(`
        **Escolha a categoria:**

        :notes: **Musicas: !musicasinfo**

        :moneybag: **Economia: !economiainfo**

        :rocket: **Util: !utilinfo**
        `)
        .setTimestamp()
        .setColor(`#00FF7F`)
    message.react('❤️');
    message.channel.send({
        embeds: [exampleEmbed]
    });
    console.log(`Comando help: foi executado por ${message.author.tag}`.green);
}