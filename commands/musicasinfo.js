const {
    MessageEmbed
} = require('discord.js')
exports.run = async (client, message, args) => {
    const exampleEmbed = new MessageEmbed()
        .setTitle('**Back+ musica !**')
        .setDescription(`**▶ !Play: Executa alguma musica
            Exemplo: !play nomedamusica

        ⏹ !Pause: Pausa alguma musica
            Exemplo: !pause
            
        ⏸ !Rplay: Despause alguma musica
            Exemplo: !rplay 
            
        ⏯ !Skip: Pula alguma musica
            Exemplo: !skip**
        `)
    message.channel.send({
        embeds: [exampleEmbed]
    });
    console.log(`Comando mscinfo: foi executado por ${message.author.tag}`.green);
}