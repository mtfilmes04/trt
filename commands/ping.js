const {
    MessageEmbed
} = require('discord.js')




exports.run = async (client, message, args) => {
    const exampleEmbed = new MessageEmbed()
        .setTitle('**Back+ ping**')
        .setDescription(`
        :satellite_orbital: Ping: ${Date.now() - message.createdTimestamp}
        :satellite: Api ping: ${Math.round(client.ws.ping)}
        `)
        .setColor('#303434')
        .setThumbnail(`https://backcomp.s3.br-sao.cloud-object-storage.appdomain.cloud/BothUnsightlyBeaver-max-1mb.gif`)
    message.channel.send({
        embeds: [exampleEmbed]
    });
    console.log(`Comando Ping: foi executado por ${message.author.tag}`.green);
}