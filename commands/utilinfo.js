const {
    MessageEmbed
} = require('discord.js')
exports.run = async (client, message, args) => {
    const exampleEmbed = new MessageEmbed()
        .setTitle('**Back+ util!**')
        .setDescription(`**🛡 !Ban: Bane alguma pessoa
            Exemplo: !ban @pessoa 
        💬 !Mute: Mute alguma pessoa 
            Exemplo: !mute @pessoa tempo
        🪛  !limpar: limpe mensagens
            Exemplo: !limpar quantia
        📡 !lembrete: peça pro bot lembrar algum
            Exemplo: !lembrete @mensagem @tempo
            Obs: Use tempos como 10s / 10m / 10h
            se o bot reiniciar você será não sera
            avisado !
        **
        `)
    message.channel.send({
        embeds: [exampleEmbed]
    });
    console.log(`Comando utilinfo: foi executado por ${message.author.tag}`.green);
}