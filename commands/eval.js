const {
    MessageEmbed
} = require("discord.js");
const owner = ["813187818305617931"]; // PUT YOUR ID OR IT WON'T LET YOU EVAL...
//if you want more users to use this command then ["YOUR DISCORD ACCOUNT ID","ANOTHER ID", "ANOTHER"] And so On...

module.exports = {
    name: "eval",
    description: "Run A Whole Code With This Command!",
    category: "owner",
    usage: "eval <code>",

    run: async (client, message, args) => {
        //Eval Command(Not to be made public btw!)
        if (message.author.id != owner) {
            return message.channel.send("Apenas o dono pode usar")
        }
        try {
            const code = args.join(" ");
            if (!code) {
                return message.channel.send("Oque voce quer executar")
            }

            let evaled = eval(code);

            if (typeof evaled !== "string")
                evaled = require("util").inspect(evaled);

            let embed = new MessageEmbed()
                .setAuthor("Eval", message.author.avatarURL(), e, e)
                .addField("Input", `\`\`\`${code}\`\`\``)
                .addField("Output", `\`\`\`${evaled}\`\`\``)
                .setColor("GREEN")

            message.channel.send({
                embeds: [embed]
            });
        } catch (err) {
            message.channel.send(`\`ERROR\` \`\`\`\n${err}\`\`\``);
        }
    }
}