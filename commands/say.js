const {
    MessageEmbed
} = require("discord.js")
const colors = require('colors');

const rq = require('../botclient')


exports.run = async (client, message, args) => {

    if (!message.member.permissions.has("MANAGE_MESSAGES"))
        return message.reply(
            ` ${"`Você não possui a permissão MANAGE_MESSAGES`"}`
        );
    if (!args[0]) return message.channel.send("`Escreva um texto depois do comando.`")

    const exampleEmbed = new MessageEmbed()
        .setTitle(args.join(" "))
        .setColor(`#00FF7F`)

        .setDescription(`Esta mensagem foi enviada por ${message.author.tag}`, )
    message.channel.send({
        embeds: [exampleEmbed]
    });
    console.log(`Comando Say: foi executado por ${message.author.tag}`.green);
}