const Discord = require('discord.js');

exports.run = async (client, message, args) => {

    var list = [
        'https://astralbot-premium.s3.br-sao.cloud-object-storage.appdomain.cloud/sla.gif'
    ];

    var rand = list[Math.floor(Math.random() * list.length)];
    let user = message.mentions.users.first() || client.users.cache.get(args[0]);
    if (!user) {
        return message.reply('`Qual membro você irá dar um abraço ?`');
    }

    let avatar = message.author.displayAvatarURL({
        format: 'jpg'
    });
    const embed = new Discord.MessageEmbed()
        .setColor('#8A2BE2')
        .setDescription(`O Membro ${message.author} Abraçou ${user}`)
        .setImage(rand)
        .setTimestamp()
        .setFooter(`Executado por: ${message.author.tag}`, message.author.displayAvatarURL({
            format: "jpg"
        }));
    await message.channel.send({
        embeds: [embed]
    });
    console.log(`Comando abraço: foi executado por ${message.author.tag}`.green);
}