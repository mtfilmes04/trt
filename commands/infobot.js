const { MessageEmbed } = require('discord.js')
let config = require('../prefixo/config.json')
var ip = require('ip');

exports.run = async (client, message, args) => {
    const exampleEmbed = new MessageEmbed()
    .setTitle('Back+ Info !')
    .setDescription(`
    Prefixo: ${config.prefix}(args)
    Dns: 0.0.0.0:0000 (localhost)
    Ip: ${ip.address()} (public)
    `)
    message.channel.send({
        embeds: [exampleEmbed]
    });
}